/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <termios.h>
#include <time.h>
#include "mx-app-wdt.h"

void print_cmd(unsigned char *buf, int len) {
	int i;
	for (i=0; i<len; i++)
		printf("0x%02X ", buf[i]);
	printf("\n");
}

void sleep_ms( int milliseconds) {
#if _POSIX_C_SOURCE >= 199309L
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
#else
	usleep(milliseconds * 1000);
#endif
}

int validate_cmd_checksum(unsigned char *cmd, int data_len) {
	unsigned char hdr_chk = 0, data_chk = 0;
	int i;

	hdr_chk = cmd[0] + cmd[1] + cmd[2] + cmd[3] + cmd[4] + cmd[5];
	hdr_chk = 0xff - (hdr_chk & 0xff);
	if (hdr_chk != cmd[6]){
		return 0;
	}

	if(cmd[5]){
		for (i = DATA_HEAD; i < data_len - 1; i++){
			data_chk += cmd[i];
		}
		data_chk = 0xff - (data_chk & 0xff);
		if (data_chk != cmd[data_len-1]){
			return 0;
		}
	}
	return 1;
}

static int set_tty(int fd) {
	struct termios termio;
	int ret;

	ret = tcgetattr(fd, &termio);
	if (ret < 0) {
		printf("Get tty attr failed: %s\n", strerror(errno));
		return ret;
	}

	termio.c_iflag = IGNBRK;
	termio.c_oflag = 0;
	termio.c_lflag = 0;
	termio.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
	termio.c_cflag &= ~CRTSCTS;
	termio.c_iflag &= ~(IXON|IXOFF|IXANY);

	termio.c_cc[VMIN] = 1;
	termio.c_cc[VTIME] = 5;
	termio.c_cflag &= ~(PARENB | PARODD);
	termio.c_cflag &= ~CSTOPB;

	ret = tcsetattr(fd, TCSANOW, &termio);
	if (ret < 0) {
		printf("Set tty attr failed: %s\n", strerror(errno));
		return ret;
	}

	ret = tcflush(fd, TCIOFLUSH);
	if (ret < 0) {
		printf("Flush tty failed: %s\n", strerror(errno));
		return ret;
	}

	return ret;
}

static int open_tty(char *ttyname) {
	int fd;

	fd = open(ttyname, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd < 0) {
		printf("Open %s: %s\n", ttyname, strerror(errno));
		return -1;
	}

	if (set_tty(fd) < 0) {
		printf("Set tty failed: %s\n", strerror(errno));
		close(fd);
		return -1;
	}
	return fd;
}


int make_packet(unsigned char *send_buf, char *cmd_hdr, int data_len, char *data) {
	unsigned char hdr_checksum = 0, data_checksum = 0;
	int i;

	for (i=0; i<HDR_LEN; i++)
		hdr_checksum += cmd_hdr[i];
	hdr_checksum = 0xff - ((hdr_checksum + (unsigned char)data_len) & 0xff);
	for (i=0; i< data_len; i++)
		data_checksum += data[i];
	data_checksum = 0xff - (data_checksum & 0xff);

	memcpy(send_buf, cmd_hdr, 5);
	send_buf[HDR_LEN] = data_len;
	send_buf[HDR_LEN+1] = hdr_checksum;
	if (data != NULL) {
		memcpy(send_buf + DATA_HEAD, data, data_len);
		send_buf[DATA_HEAD + data_len] = data_checksum;
		return DATA_HEAD + data_len + 1;
	}
	return DATA_HEAD;
}

int cmd_send(unsigned char *read_buf, unsigned char *send_buf, int pkt_len) {
	int fd, ret;
	int ack;

/* MCU_DEVICE is MCU UART interface, according to hardware setting */
#ifdef MCU_DEVICE	
	fd = open_tty(MCU_DEVICE);
	if (fd < 0) {
		printf("open tty failed\n");
		return -1;
	}
#else
#error "Please define MCU device name!"
#endif
	#ifdef DEBUG
	print_cmd(send_buf, pkt_len);
	#endif
	ret = write(fd, send_buf, pkt_len);
	if (ret < 0) {
		printf("write failed\n");
		return -1;
	}
	sleep_ms(200);
	memset(read_buf, 0, MAX_PKT_LEN);
	ret = read(fd, read_buf, MAX_PKT_LEN);
	if (ret < 0){
		printf("response failed %s\n", strerror(errno));
		return -1;
	}

	ack = read_buf[0] & 0xFF;

	// 0x15 (Negative ACK packet)
	if (ack == 0x15) {
		printf("command is not support\n");
		return -1;
	}

	// 0x06 (Acknowledge packet)
	if (ack != 0x06) {
		printf("response failed\n");
		return -1;
	}

	if (!validate_cmd_checksum(read_buf, ret)) {
		printf("response checksum error\n");
		return -1;
	}

	#ifdef DEBUG
	print_cmd(read_buf, ret);
	#endif

	sleep_ms(200);
	close(fd);

	return ret;
}

int mx_set_app_wdt_mode(int mode, int timeout) {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret;
	char buf;
	char data_buf[2];

	if (0 == mode) {
		/* disable app wdt */
		buf = (unsigned char)mode;
		ret = make_packet(send_buf, APP_WDT_MODE_HDR, 1, &buf);
		ret = cmd_send(recv_buf, send_buf, ret);

		if (ret < 0) {
			return -1;
		}
		printf("Set APP WDT Mode: %x\n", recv_buf[DATA_HEAD]);
	} else if (mode == 1 && 5 <= timeout && timeout <= 15) {
		/* enable and set timeout for app wdt */
		data_buf[0] = (unsigned char)mode;
		data_buf[1] = (unsigned char)timeout;
		ret = make_packet(send_buf, APP_WDT_MODE_HDR, 2, data_buf);
		ret = cmd_send(recv_buf, send_buf, ret);

		if (ret < 0) {
			return -1;
		}
		printf("Set APP WDT Mode: %x\n", recv_buf[DATA_HEAD]);
		printf("Set APP WDT Timeout: %d\n", recv_buf[DATA_HEAD+1]);
	} else {
		printf("Please check input variables. Exit.\n");
		return -1;
	}

	return ret;
}

int mx_get_app_wdt_mode() {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret;

	ret = make_packet(send_buf, APP_WDT_MODE_HDR, 1, "\x3F");
	ret = cmd_send(recv_buf, send_buf, ret);

	if (ret < 0) {
		return -1;
	}

	printf("Get APP WDT Mode: %x\n", recv_buf[DATA_HEAD]);
	printf("Get APP WDT Timeout: %d\n", recv_buf[DATA_HEAD+1]);
	return ret;
}

int mx_set_app_wdt_reset_mode(int resetMode) {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret;
	char buf;

	if (0 <= resetMode && resetMode <= 1) {
		buf = (unsigned char)resetMode;
		ret = make_packet(send_buf, APP_WDT_RESET_HDR, 1, &buf);
		ret = cmd_send(recv_buf, send_buf, ret);

		if (ret < 0) {
			return -1;
		}
		printf("Set APP Watchdog Reset Mode: %x\n", recv_buf[DATA_HEAD]);
	} else {
		return -1;
	}

	return ret;
}

int mx_get_app_wdt_reset_mode() {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret;

	ret = make_packet(send_buf, APP_WDT_RESET_HDR, 1, "\x3F");
	ret = cmd_send(recv_buf, send_buf, ret);

	if(ret < 0){
		return -1;
	}

	printf("Get APP Watchdog Reset Mode: %x\n", recv_buf[DATA_HEAD]);
	return ret;
}

int mx_set_app_wdt_relay_mode(int relayMode) {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret;
	char buf;

	if (0 <= relayMode && relayMode <= 2) {
		buf = (unsigned char)relayMode;
		ret = make_packet(send_buf, APP_WDT_RELAY_HDR, 1, &buf);
		ret = cmd_send(recv_buf, send_buf, ret);

		if (ret < 0) {
			return -1;
		}
		printf("Set APP Watchdog Relay Mode: %x\n", recv_buf[DATA_HEAD]);
	} else {
		return -1;
	}

	return ret;
}

int mx_get_app_wdt_relay_mode() {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret;

	ret = make_packet(send_buf, APP_WDT_RELAY_HDR, 1, "\x3F");
	ret = cmd_send(recv_buf, send_buf, ret);
	if (ret < 0) {
		return -1;
	}

	printf("Get APP Watchdog Relay Mode: %x\n", recv_buf[DATA_HEAD]);
	return ret;
}
