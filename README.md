# Moxa APP Watchdog Utility for V3000 series

Moxa APP Watchdog Utility for V3000 series

## Build code

- Build source code

```
apt update && apt install build-essential
git clone https://gitlab.com/moxa/open-source/linux/packages/moxa-app-wdt-utility.git -b master
cd moxa-app-wdt-utility
make MCU_DEVICE=/dev/ttyS2
make install
```
The value of MCU_DEVICE, please refer to [MCU uart port device Name](#MCU-uart-port-device-Name)

- Add and enable systemd service

```
cp mx-app-wdtd.service mx-disable-app-wdt.service /lib/systemd/system/
systemctl enable mx-app-wdtd.service
systemctl enable mx-disable-app-wdt.service

# start and check service status
systemctl start mx-app-wdtd.service
systemctl status mx-app-wdtd.service
```

the `mx-app-wdtd` service status shall be like:
```
● mx-app-wdtd.service - Moxa app watchdog daemon service
     Loaded: loaded (/lib/systemd/system/mx-app-wdtd.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-06-29 02:39:03 EDT; 38min ago
   Main PID: 5463 (mx-app-wdtd)
      Tasks: 1 (limit: 9236)
     Memory: 148.0K
        CPU: 134ms
     CGroup: /system.slice/mx-app-wdtd.service
             └─5463 /usr/sbin/mx-app-wdtd

Jun 29 02:39:03 debian systemd[1]: Started Moxa app watchdog daemon service.
```

## Usage

```
Usage:
        mx-app-wdt-ctl [Options]...
Options:
        -a, --appwdt_mode
                Get app wdt mode and timeout status
        -a, --appwdt_mode [0|1] [5:15]
                Set app wdt mode and timeout [0:disable|1:enable] [5~15 timeout in sec]
        -w, --appwdt_resetmode
                Get app wdt reset mode
        -w, --appwdt_resetmode [0|1]
                Set app wdt reset mode (0:not reset|1:reset)
        -m, --appwdt_relaymode
                Get app wdt relay mode
        -m, --appwdt_relaymode [0|1|2]
                Set app wdt relay mode (0:connect|1:disconnect|2:by-pass)
```

## MCU uart port device Name
|          | Device Node Name |
|----------|------------------|
| V3000    | /dev/ttyS2       |
| CIM-7100 | /dev/ttyMUE0     |