/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */


#define HDR_LEN			5
#define DATA_HEAD		7
#define MAX_PKT_LEN		40
#define TIMEOUT			5
#define MAX_SH_SIZE 		128
#define	APP_WDT_TICK_DELAY	1

/* CMD format: chk bit(2) + cmd id(3) + data len(1) + hdr checksum(1) + data(data len) + data checksum(1) */
/* checksum = 0xff - (data sum & 0xff) */
#define APP_WDT_MODE_HDR	"\x07\xFF\x41\x57\x54"
#define APP_WDT_RESET_HDR	"\x07\xFF\x41\x57\x52"
#define APP_WDT_RELAY_HDR	"\x07\xFF\x41\x57\x4C"
#define APP_WDT_TICK		"\x07\xFF\x41\x57\x54\x00\x0D"


void print_cmd(unsigned char *buf, int len);

int mx_set_app_wdt_mode(int mode, int timeout);
int mx_get_app_wdt_mode();

int mx_set_app_wdt_reset_mode(int resetMode);
int mx_get_app_wdt_reset_mode();

int mx_set_app_wdt_relay_mode(int relayMode);
int mx_get_app_wdt_relay_mode();

