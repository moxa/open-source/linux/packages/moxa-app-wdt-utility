CROSS_COMPILE =
CC := $(CROSS_COMPILE)gcc
STRIP := $(CROSS_COMPILE)strip
MCU_DEVICE =
CFLAGS:=-Wall -fPIC -DMCU_DEVICE=\"$(MCU_DEVICE)\"
LIB := mx-app-wdt
UTIL := mx-app-wdt-ctl
DAEMON := mx-app-wdtd
DESTDIR ?= /

all: clean $(UTIL)

$(LIB).a:
	@if [ -z $(MCU_DEVICE) ]; then \
		echo "Please define MCU device name! e.g. MCU_DEVICE=/dev/ttyS2"; \
		exit 1; \
	fi
	gcc $(CFLAGS) $(DEBUG) -c $(LIB).c 
	ar r $(LIB).a $(LIB).o

$(UTIL): $(UTIL).c $(LIB).a lib$(LIB).so
	$(CC) $(DEBUG) -o $(UTIL) $(UTIL).c -L. $(LIB).a
	$(CC) $(DEBUG) $(CFLAGS) -o $(DAEMON) $(DAEMON).c -L. $(LIB).a
	$(STRIP) -s $(UTIL)
	$(STRIP) -s $(DAEMON)

lib%.so: $(LIB).c
	$(CC) $(CFLAGS) -shared -I.  -o $@ $< 


install:
	/usr/bin/install -d $(DESTDIR)/sbin
	/usr/bin/install $(UTIL) $(DESTDIR)/sbin/$(UTIL)
	/usr/bin/install $(DAEMON) $(DESTDIR)/sbin/$(DAEMON)

debug: DEBUG = -DDEBUG
debug: clean $(UTIL)

.PHONY: clean
clean:
	rm -f $(UTIL) $(DAEMON) *.a *.o *.so $(DESTDIR)/sbin/$(UTIL) $(DESTDIR)/sbin/$(DAEMON) 

