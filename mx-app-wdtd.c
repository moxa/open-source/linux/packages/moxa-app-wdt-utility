/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include "mx-app-wdt.h"

void usage()
{
	printf("Usage:\n");
	printf("	mx-app-wdtd [Options]...\n");
	printf("Options:\n");
	printf("	-t, --time_interval\n");
	printf("		Set app wdt tick daemon time interval (sec).\n");
	printf("	-h, --help\n");
	printf("		Show this help message.\n");
	printf( "\n");
}

static int set_tty(int fd)
{
	struct termios termio;

	tcgetattr(fd, &termio);

	termio.c_iflag = IGNBRK;
	termio.c_oflag = 0;
	termio.c_lflag = 0;
	termio.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
	termio.c_cflag &= ~CRTSCTS;
	termio.c_iflag &= ~(IXON|IXOFF|IXANY);

	termio.c_cc[VMIN] = 1;
	termio.c_cc[VTIME] = 5;
	termio.c_cflag &= ~(PARENB | PARODD);
	termio.c_cflag &= ~CSTOPB;

	tcsetattr(fd, TCSANOW, &termio);
	tcflush(fd, TCIOFLUSH);
	return fd;
}

static int open_tty(char *ttyname)
{
	int fd;

	fd = open(ttyname, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd < 0) {
		fprintf(stderr, "Open %s: %s\n", ttyname, strerror(errno));
		return -1;
	}

	if (set_tty(fd) < 0) {
		fprintf(stderr, "Set tty failed: %s\n", strerror(errno));
		close(fd);
		return -1;
	}
	return fd;
}

int run_tick_deamon(int input_time) {
	int fd, delay_time;

/* MCU_DEVICE is MCU UART interface, according to hardware setting */
#ifdef MCU_DEVICE
	fd = open_tty(MCU_DEVICE);

	if (fd < 0) {
		return -1;
	}
#else
#error "Please define MCU device name!"
#endif

	if (input_time > 0) {
		delay_time = input_time;
		printf("Use tick time interval for %d sec.\n", delay_time);
	} else if (input_time < 0) {
		printf("Tick time interval must be greater than zero.\n");
		close(fd);
		return -1;
	} else {
		delay_time = APP_WDT_TICK_DELAY;
		printf("Use default tick time interval for %d sec.\n", delay_time);
	}

	/* to avoid exclude other processes from using the port concurrently */
	ioctl(fd, O_EXCL);

	/* simple tick MCU uart port */
	do {
		write(fd, APP_WDT_TICK, 7);
		sleep(delay_time);
	} while (1);

	close(fd);
	return 0;
}

int main(int argc, char *argv[])
{
	int c;
	struct option long_options[] = {
		{"help", no_argument, 0, 'h'},
		{"time_interval", no_argument, 0, 't'},
		{0, 0, 0, 0}
	};

	if (argc > 3) {
		usage();
		return 1;
	}

	c = getopt_long(argc, argv, "ht", long_options, NULL);
	switch (c) {
		case 'h':
			usage();
			exit(0);
		case 't':
			if (argv[optind]) {
				run_tick_deamon(atoi(argv[optind]));
			}
		default:
			run_tick_deamon(0);
	}

	return 0;
}



