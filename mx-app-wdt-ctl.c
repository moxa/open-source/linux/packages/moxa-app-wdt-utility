/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include "mx-app-wdt.h"

void usage()
{
	printf("Usage:\n");
	printf("	mx-app-wdt-ctl [Options]...\n");
	printf("Options:\n");
	printf("	-a, --appwdt_mode\n");
	printf("		Get app wdt mode and timeout status\n");
	printf("	-a, --appwdt_mode [0|1] [5:15]\n");
	printf("		Set app wdt mode and timeout [0:disable|1:enable] [5~15 timeout in sec]\n");
	printf("	-w, --appwdt_resetmode\n");
	printf("		Get app wdt reset mode\n");
	printf("	-w, --appwdt_resetmode [0|1]\n");
	printf("		Set app wdt reset mode (0:not reset|1:reset)\n");
	printf("	-m, --appwdt_relaymode\n");
	printf("		Get app wdt relay mode\n");
	printf("	-m, --appwdt_relaymode [0|1|2]\n");
	printf("		Set app wdt relay mode (0:connect|1:disconnect|2:by-pass)\n");
	printf( "\n");
}

int main(int argc, char *argv[])
{
	int c, ret;
	struct option *options;

	struct option long_options[] = {
		{"help", no_argument, 0, 'h'},
		{"appwdt_mode", no_argument, 0, 'a'},
		{"appwdt_resetmode", no_argument, 0, 'w'},
		{"appwdt_relaymode", no_argument, 0, 'm'},
		{0, 0, 0, 0}
	};

	if (argc == 1 || argc > 4) {
		usage();
		return 1;
	}

	c = getopt_long(argc, argv, "hawm", long_options, NULL);
	switch (c) {
		case 'h':
			usage();
			exit(0);
		case 'a':
			if (argv[optind]) {
				if (argc == 4) {
					mx_set_app_wdt_mode(atoi(argv[optind]), atoi(argv[optind+1]));
				} else {
					printf("Please check input variables. Exit.\n");
					return 1;
				}
			} else {
				mx_get_app_wdt_mode();
			}
			break;
		case 'w':
			if (argv[optind]) {
				mx_set_app_wdt_reset_mode(atoi(argv[optind]));
			} else {
				mx_get_app_wdt_reset_mode();
			}
			break;
		case 'm':
			if (argv[optind]) {
				mx_set_app_wdt_relay_mode(atoi(argv[optind]));
			} else {
				mx_get_app_wdt_relay_mode();
			}
			break;

		default:
			usage();
			exit(99);
	}

	return 0;
}

